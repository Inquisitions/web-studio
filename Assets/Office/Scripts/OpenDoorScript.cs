﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Скрипт открытия дверей
/// </summary>
public class OpenDoorScript : MonoBehaviour
{
    public enum Direction { Inside, Outside }; // Типы открывания двери на(от) себя

    [SerializeField] private Direction m_Direction = Direction.Inside; // Направление открытия

    private bool m_IsNearPlayer = false;        // Состояние близости игрока
    private bool m_StateDoor = true;            // Состояние двери (true = закрыта)
    private bool m_AnimationIsPlaying = false;  // Проигрывается ли анимация открытия на данный момент
    private Vector3 m_StartRotatePosition;      // Стартовое значение поворота

    void OnTriggerEnter(Collider c)
    {
        print("Trigger enter");
        if (c.gameObject.tag == "Player")
        {
            m_IsNearPlayer = true;
            string text = !m_StateDoor ? "Нажмите F чтобы закрыть дверь" : "Нажмите F чтобы открыть дверь";
            CanvasController.Controller.ActionText(text, true);
        }
    }

    void OnTriggerExit(Collider c)
    {
        print("Trigger exit");
        m_IsNearPlayer = false;
        CanvasController.Controller.ActionText("Press F to open door", false);
    }

    void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(GameKey.ActionKey) && !m_AnimationIsPlaying)
        {
            m_StateDoor = !m_StateDoor;
            ActionDoor(m_StateDoor);
        }
    }

    void Start()
    {
        m_StartRotatePosition = transform.localEulerAngles;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 direct = m_Direction == Direction.Outside ? Vector3.forward : Vector3.back;
        direct = transform.TransformDirection(direct);
        Gizmos.DrawLine(transform.transform.position, transform.transform.position + direct);
    }

    /// <summary>
    /// Метод открытия/закрытия дверей
    /// </summary>
    /// <param name="isState"> Передаваемое состояние двери </param>
    public void ActionDoor(bool isState)
    {
        m_StateDoor = isState;
        if (!m_AnimationIsPlaying)
            StartCoroutine(ChangePositionDoor());
    }

    /// <summary>
    /// Смена позиции двери в отдельной сопрограмме
    /// </summary>
    /// <returns></returns>
    private IEnumerator ChangePositionDoor()
    {
        m_AnimationIsPlaying = true;
        Vector3 startPosition;
        Vector3 endPosition;

        if (m_Direction != Direction.Inside)
        {
            startPosition = m_StateDoor ? transform.localEulerAngles : m_StartRotatePosition;
            endPosition = m_StateDoor ? m_StartRotatePosition : m_StartRotatePosition + new Vector3(0, 90, 0);
        }
        else
        {
            startPosition = m_StateDoor ? transform.localEulerAngles : m_StartRotatePosition;
            endPosition = m_StateDoor ? m_StartRotatePosition : m_StartRotatePosition + new Vector3(0, -90, 0);
        }
        print("Start position: " + startPosition.ToString());
        print("End position: " + endPosition.ToString());
        bool isPlay = true;
        while (isPlay)
        {
            float distance1 = Vector3.Distance(transform.localEulerAngles, endPosition);
            print("Distance 1: " + distance1.ToString());
            transform.localEulerAngles += ((endPosition - startPosition) * Time.deltaTime);
            print((new Vector3(0, 0, 0) - new Vector3(0, 90, 0)).ToString());
            float distance2 = Vector3.Distance(transform.localEulerAngles, endPosition);
            print("Distance 2: " + distance2.ToString());
            isPlay = distance1 > distance2;
            yield return new WaitForEndOfFrame();
        }
        m_AnimationIsPlaying = false;
    }

    #region Set&Get methods
    public bool IsNearPlayer { set => m_IsNearPlayer = value; get => m_IsNearPlayer; }
    public bool IsAnimationPlaying { get => m_AnimationIsPlaying; }
    public bool IsCloseDoor { get => m_StateDoor; }
    #endregion
}
