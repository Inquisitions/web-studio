﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sender
{
    private ulong m_Id;             // Номер сообщения
    private string m_Text;          // Текст отправителя
    private string m_Initials;      // Инициалы отправителя
    private string m_EMail;         // Почта отправителя
    private string m_Time;          // Время отправки

    public static ulong messageId = 0;

    public Sender()
    {
        m_Id = messageId++;
    }

    public string Text { set => m_Text = value; get => m_Text; }
    public string Initials { set => m_Initials = value; get => m_Initials; }
    public string EMail { set => m_EMail = value; get => m_EMail; }
    public string Time { set => m_Time = value; get => m_Time; }
}
