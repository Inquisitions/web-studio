﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MailScript : MonoBehaviour
{
    [SerializeField] private GameObject m_PrefabItem;   // Шаблон элемента
    [SerializeField] private float m_Offset = 7.5f;

    private List<Sender> m_Senders = new List<Sender>(); // Список отправителей

    void Start()
    {
        AddMailSender(new Sender { Initials = "John Miller" });
        AddMailSender(new Sender { Initials = "Kate Shidt"  });
        AddMailSender(new Sender { Initials = "Lilit Scare" });
        AddMailSender(new Sender { Initials = "Обэма блять" });
        AddMailSender(new Sender { Initials = "John Miller" });
        AddMailSender(new Sender { Initials = "Kate Shidt" });
        AddMailSender(new Sender { Initials = "Lilit Scare" });
        AddMailSender(new Sender { Initials = "Обэма блять" });
    }

    public void AddMailSender(Sender sender)
    {
        GameObject obj = Instantiate(m_PrefabItem, gameObject.transform) as GameObject;
        RectTransform transform = obj.GetComponent<RectTransform>();
        transform.SetParent(gameObject.GetComponent<RectTransform>());    // Добавление как child
        float y = -m_Offset + (-m_Offset - transform.rect.height) * m_Senders.Count;    // Вычисление по y
        transform.localPosition = new Vector3(transform.localPosition.x, y);            // Установка позиции

        RectTransform transformParent = gameObject.GetComponent<RectTransform>();
        print((m_Offset + transform.rect.height) * m_Senders.Count);
        transformParent.rect.Set(transformParent.rect.x, transformParent.rect.y,
            transformParent.rect.width, (m_Offset + transform.rect.height) * m_Senders.Count);

        FillDataItem(ref sender, obj);  // Заполним графическую часть

        m_Senders.Add(sender);
    }

    /// <summary>
    /// Заполнение графической части итема
    /// </summary>
    /// <param name="s">Данные об отправителе</param>
    /// <param name="obj">Объект родитель</param>
    private void FillDataItem(ref Sender s, GameObject obj)
    {
        obj.GetComponent<RectTransform>().GetChild(1).GetComponent<UnityEngine.UI.Text>().text = s.Initials;
    }

    /// <summary>
    /// Добавить отправителя сообщения
    /// </summary>
    /// <param name="initials">Инициалы</param>
    /// <param name="mail">Почта</param>
    /// <param name="time">Время отправки</param>
    /// <param name="text">Текст</param>
    public void AddMailSender(string initials, string mail, string time, string text)
    {
        m_Senders.Add(new Sender
        {
            Initials = initials,
            EMail = mail,
            Time = time,
            Text = text
        });
    }
}
