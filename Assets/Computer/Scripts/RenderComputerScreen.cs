﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderComputerScreen : MonoBehaviour
{
    [SerializeField] private Material m_Material_Screen;

    private GameObject m_Owner;
   

    void Start()
    {
        m_Owner = GetComponent<GameObject>();
    }

    public void SetScreenShot()
    {
        string path = Application.dataPath + "\\Web Studio_Data\\Resources\\screen.png";
        print(path.ToString());
        ScreenCapture.CaptureScreenshot(path);
        m_Material_Screen.color = Color.white;
        m_Material_Screen.SetTexture(path, m_Material_Screen.mainTexture);
    }
}
