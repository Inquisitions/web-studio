﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractMotion : MonoBehaviour
{
    [SerializeField] private float m_Speed = 6;
    [SerializeField] private string m_NameMotion;

    private bool m_ActiveCurrentMotion = false;

    public abstract void Motion(ref CharacterController c);

    public float Speed { set => m_Speed = value; get => m_Speed; }
    public string Name { set => m_NameMotion = value; get => m_NameMotion; }
}
