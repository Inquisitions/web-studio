﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkMotion : AbstractMotion
{

    ///u0
    private float startSpeed = 0;
    private float time = 0;

    public override void Motion(ref CharacterController c)
    {
        Vector3 directMotion = Vector3.zero;
        float horVelocity = Input.GetAxis("Horizontal"),
              verVelocity = Input.GetAxis("Vertical");
        directMotion = new Vector3(horVelocity, 0, verVelocity);

        if (c.isGrounded)
        {
            // Вычисление направления ходьбы

            directMotion = Vector3.ClampMagnitude(directMotion, 1);

            directMotion = transform.TransformDirection(directMotion);
            // Отвязка от FPS
            float currentSpeed = Vector3.Distance(Vector3.zero, directMotion);
            directMotion *= Speed;

        }
        else
        {
            time += Time.deltaTime;
            float speedXZ = startSpeed * Mathf.Sin(45);
            float speedY  = startSpeed   * Mathf.Cos(45) + Physics.gravity.y * time;
            directMotion.y = startSpeed * Mathf.Cos(45) * time + Physics.gravity.y * Mathf.Pow(time, 2) / 2;
        }

        directMotion *= Time.deltaTime;
        // Преобразование координат
        // Перемещение
        c.Move(directMotion);
    }
    
}
