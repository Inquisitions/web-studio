﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Скрипт поворота головы в различные стороны
/// </summary>
public class MouseLookController : MonoBehaviour
{
    [SerializeField] private float m_SensetiveHor = 10f;    // Скорость вращения камеры по горизонтали
    [SerializeField] private float m_SensetiveVer = 10f;    // Скорость вращения камеры по вертикали
    [SerializeField] [Range(0, 1)] private float m_WeightBody;
    [SerializeField] [Range(0, 1)] private float m_TotalWeight;

    private Animator m_Animator;            // Аниматор персонажа
    //private Vector3 m_Target;               // Позиция просмотра персонажа
    private bool isHeadRotate = false;      // В состоянии вращения головой

    GameObject m_TargetObject;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_TargetObject = new GameObject();
        m_TargetObject.transform.SetParent(gameObject.transform);
        m_TargetObject.transform.localPosition = new Vector3(0, 2, 5);
    }

    // Update is called once per frame
    void Update()
    {
        KeyListener();

        // Возможно ограничение скорости

        float velocityMouseX = Input.GetAxis("Mouse X") * m_SensetiveHor;
        float velocityMouseY = Input.GetAxis("Mouse Y") * Time.deltaTime * m_SensetiveVer;

        if (isHeadRotate)
            m_TargetObject.transform.localPosition += new Vector3(velocityMouseX * Time.deltaTime, 0, 0);
        else
        {
            gameObject.transform.Rotate(new Vector3(0, velocityMouseX / 2.0f, 0));
        }


        m_TargetObject.transform.position += new Vector3(0, velocityMouseY, 0);
    }

    void OnAnimatorIK(int layerIndex)
    {
        //Vector3 lookPosition = gameObject.transform.position + m_Target;   // Вычисление точки просмотра
        m_Animator.SetLookAtWeight(m_TotalWeight, m_WeightBody, 0.8f);
        m_Animator.SetLookAtPosition(m_TargetObject.transform.position);
    }

    private void KeyListener()
    {
        GameKey.KeyPressedAndUp(GameKey.RotateHead, ref isHeadRotate);
        // Выравнивание головы по центру
        if (Input.GetKeyDown(GameKey.ViewAligment))
            m_TargetObject.transform.localPosition = new Vector3(0, 2, 5);
    }
}
