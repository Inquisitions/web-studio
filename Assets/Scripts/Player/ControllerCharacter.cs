﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Скрипт управления движениями
/// </summary>
public class ControllerCharacter : MonoBehaviour
{
    // Контейнер движений
    MotionContainer m_Motions = new MotionContainer();

    private CharacterController m_CharacterControler = null;

    #region Constants
    private const string WALK   = "Walk";
    #endregion

    void Start()
    {
        AbstractMotion[] mots = GetComponents<AbstractMotion>();
        m_CharacterControler = GetComponent<CharacterController>();
        // Формирование имени
        foreach (AbstractMotion m in mots)
            m_Motions.Add(m.Name, m);
    }
    

    void Update()
    {
        AbstractMotion motion;
        if (m_Motions.Find(WALK, out motion))
        {
            motion.Motion(ref m_CharacterControler);
            return;
        }
    }
}
