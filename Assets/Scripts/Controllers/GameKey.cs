﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameKey
{
    public static KeyCode ActionKey = KeyCode.F;            // Клавиша действия
    public static KeyCode RotateHead = KeyCode.LeftAlt;     // Клавиша вращения головы
    public static KeyCode ViewAligment = KeyCode.B;         // Клавиша центрирования головы

    public static void KeyPressedAndUp(KeyCode key, ref bool value)
    {
        if (Input.GetKey(key))
            value = true;
        else if (Input.GetKeyUp(key))
            value = false;
    }
}
