﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CanvasController : MonoBehaviour
{

    [SerializeField] private Text m_ActionText;
    [SerializeField] private GameObject[] m_Canvases;

    private static CanvasController sController;

    // Для невозмоности использования
    private CanvasController(){}

    void Start()
    {
        sController = GetComponent<CanvasController>();
        m_ActionText.enabled = false;
        m_Canvases[0].SetActive(true);
        for (int i = 1; i < m_Canvases.Length; i++)
            m_Canvases[i].SetActive(false);

    }

    /// <summary>
    /// Установка текста для действия
    /// </summary>
    /// <param name="text">Текст для отображения</param>
    /// <param name="isActive">Активация или деактивация</param>
    public void ActionText(string text, bool isActive)
    {
        m_ActionText.text = text;
        m_ActionText.enabled = isActive;
    }

    public void ActivateComputerCanvas()
    {
        GameObject obj = Find("Computer Canvas", true);
        obj.SetActive(true);
    }

    public void ActivateHeadCanvas()
    {
        GameObject obj = Find("Head Canvas", true);
        obj.SetActive(true);
    }

    /// <summary>
    /// Поиск Холста по имени
    /// </summary>
    /// <param name="name">Имя объекта</param>
    /// <param name="isDisable">Отключить объекты</param>
    /// <returns>Объект с именем</returns>
    private GameObject Find(string name, bool isDisable)
    {
        GameObject obj = null;
        foreach (GameObject item in m_Canvases)
        {
            if (item.name == name) obj = item;
            if (isDisable) item.SetActive(false);
        }
        return obj;
    }

    public bool ActionTextEnabled { get => m_ActionText.enabled; }
    public static CanvasController Controller { get => sController; }
}
