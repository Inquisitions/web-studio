﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class СomputerController : MonoBehaviour
{
    [SerializeField] private Image m_ImageIcon;

    private GameObject m_ReferenceOpenWindow;

    public void OpenProgram(Button button)
    {
        m_ImageIcon.sprite = button.image.sprite;
        m_ImageIcon.enabled = true;
    }

    public void OpenMail(GameObject linked)
    {
        linked.SetActive(true);
        m_ReferenceOpenWindow = linked;
    }

    public void Close()
    {
        m_ImageIcon.sprite = null;
        m_ImageIcon.enabled = false;
        m_ReferenceOpenWindow.SetActive(false);
        print(gameObject.ToString());
        gameObject.GetComponent<RenderComputerScreen>().SetScreenShot();
    }
}
