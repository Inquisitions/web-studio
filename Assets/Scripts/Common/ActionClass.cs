﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActionClass : MonoBehaviour
{
    public abstract void Action();
}
