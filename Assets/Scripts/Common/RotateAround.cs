﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Скрипт перемещения по окружности вокруг какого-то объекта
/// </summary>
[RequireComponent(typeof(Transform))]
public class RotateAround : MonoBehaviour
{
    enum Plane { XY, XZ, YZ };
    [SerializeField] private Transform m_ObjectPosition;
    [SerializeField] private float m_SpeedRotation;
    [SerializeField] private Plane m_PlaneRotation = Plane.XY;
    

    private float mAngle = 0f;
    private float mDistanse;
    private ActionClass[] m_ActionDelegates;

    void Start()
    {
        mDistanse = Vector3.Distance(transform.position, m_ObjectPosition.position);
        m_ActionDelegates = GetComponents<ActionClass>();
    }
    // Update is called once per frame
    void Update()
    {
        mAngle = (mAngle + m_SpeedRotation * Time.deltaTime) % 360;
        transform.position = SolutionVectorPosition(mAngle);
        // Actions
        foreach (ActionClass item in m_ActionDelegates)
            item.Action();
    }

    void OnDrawGizmos()
    {
        for (int angle = 0; angle < 360; angle++)
        {
            Vector3 firstPosition = SolutionVectorPosition(angle);
            Vector3 secondPosition = SolutionVectorPosition(angle);
            Gizmos.DrawLine(firstPosition, secondPosition);
        }
    }

    /// <summary>
    /// Функция вычисления позиции вектора, при определённом угле
    /// </summary>
    /// <param name="angle"></param>
    /// <returns></returns>
    private Vector3 SolutionVectorPosition(float angle)
    {
        Vector3 newPosition = new Vector3();
        switch (m_PlaneRotation)
        {
            case Plane.XY:
                newPosition.x = Mathf.Cos(angle) * mDistanse;
                newPosition.y = Mathf.Sin(angle) * mDistanse;
                break;
            case Plane.XZ:
                newPosition.x = Mathf.Cos(angle) * mDistanse;
                newPosition.z = Mathf.Sin(angle) * mDistanse;
                break;
            case Plane.YZ:
                newPosition.z = Mathf.Cos(angle) * mDistanse;
                newPosition.y = Mathf.Sin(angle) * mDistanse;
                break;
        }
        return newPosition;
    }

    #region Setter and Getter
    public void SetTransform(GameObject g)  { m_ObjectPosition = g.transform; }
    public void SetTransform(Transform t)   { m_ObjectPosition = t; }
    public void SetSpeedRot(float speed)    { m_SpeedRotation = speed; }

    public Transform GetObjectTransform() { return m_ObjectPosition; }
    public Transform GetTransform() { return gameObject.transform; }
    public float GetDistanse() { return mDistanse; }
    public float GetSpeedRotation() { return m_SpeedRotation; }
    #endregion
}
