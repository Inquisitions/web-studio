﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunAction : ActionClass
{
    [SerializeField] private Color m_StartColor = new Color();
    [SerializeField] private Color m_EndColor = new Color();

    private RotateAround m_RotateComponent;

    void Start()
    {
       m_RotateComponent = GetComponent<RotateAround>();
    }

    public override void Action()
    {
        Vector3 pos = m_RotateComponent.GetTransform().position;
        Light light = gameObject.GetComponent<Light>();
        bool isActive = light.enabled;

        if (pos.y < -10 && isActive) light.enabled = false;
        else if (pos.y > -10 && !isActive) light.enabled = true;

        float ratioColor = m_RotateComponent.transform.position.y / m_RotateComponent.GetDistanse();
        light.color = ColorSolution(ratioColor);
        //light.color = new Color(MAX_COLOR, Mathf.Abs(ratioColor), Mathf.Abs(ratioColor));

        transform.LookAt(m_RotateComponent.GetObjectTransform());
    }

    /// <summary>
    /// Функция вычисления цвета на текущем шаге от 0 до 1
    /// </summary>
    /// <param name="step">Range 0-1 value</param>
    /// <returns></returns>
    private Color ColorSolution(float step)
    {
        Color color = new Color();
        Color between = m_EndColor - m_StartColor;
        color = m_StartColor + between * step;
        return color;
    }
}

