﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class MotionContainer
{
    // Пара имя - значение
    public struct MotionPair
    {
        private string m_Name;
        private AbstractMotion m_Motion;
        public MotionPair(string s, AbstractMotion a) { m_Name = s; m_Motion = a; }
        public string Name { set => m_Name = value; get => m_Name; }
        public AbstractMotion Motion { set => m_Motion = value; get => m_Motion; }
    };

    private List<MotionPair> m_Motions;

    public MotionContainer()
    {
        m_Motions = new List<MotionPair>();
    }

    public MotionContainer(string p, char separator, params AbstractMotion[] motions)
    {
        m_Motions = new List<MotionPair>();
        string[] names = p.Split(separator);
        int len = Math.Min(names.Length, motions.Length);
        for (int i = 0; i < len; i++)
            Add(names[i], motions[i]);

    }

    /// <summary>
    /// Добавление передвижения
    /// </summary>
    /// <param name="s">Имя передвижения</param>
    /// <param name="a">Скрипт передвижения</param>
    public void Add(string s, AbstractMotion a)
    {
        m_Motions.Add(new MotionPair(s, a));
    }

    public bool Find(string s, out AbstractMotion mot)
    {
        foreach (MotionPair m in m_Motions)
        {
            if (m.Name.Equals(s))
            {
                mot = m.Motion; 
                return true;
            }
        }
        mot = null;
        return false;
    }

    public override string ToString()
    {
        string retStr = "";
        foreach (MotionPair m in m_Motions)
            retStr += (m.Name + ' ');
        return retStr;
    }
}
