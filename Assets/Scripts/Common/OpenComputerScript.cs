﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenComputerScript : MonoBehaviour
{
    [SerializeField] private CanvasController m_CanvasController;

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            CanvasController.Controller.ActionText("Зайти в компьютер", true);
        }
    }

    void OnTriggerExit(Collider c)
    {
        CanvasController.Controller.ActionText("Зайти в компьютер", false);
    }

    void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(GameKey.ActionKey))
        {
            m_CanvasController.ActivateComputerCanvas();
        }
    }
}
